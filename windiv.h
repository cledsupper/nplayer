#ifndef _WINDIV_H_
#define _WINDIV_H_
#include "nflog/nflog.h"
#include <stdbool.h>
#include <ncurses.h>

typedef struct _windiv WinDiv;

// Em C++ os typedef's para definição de tipos de enums, structs e unions não é necessário
#ifndef __cplusplus
typedef union windiv_parent     windiv_parent;
typedef enum print_on_align     print_on_align;
typedef enum printline_on_align printline_on_align;
typedef enum windiv_visibility  windiv_visibility;
#endif

union windiv_parent {
  WINDOW *window;
  WinDiv *windiv;
};

enum windiv_visibility {
  WINDIV_VISIBLE,
  WINDIV_INVISIBLE,
  WINDIV_GONE
};

struct _windiv {
  /* Não toque nisso */
  const char *__struct_type;

  WINDOW *win;
  windiv_visibility visibility;
  struct _padding {
    int top;
    int bottom;
    int left;
    int right;
  } padding;
};

enum print_on_align {
  WINDIV_PRINT_ON_TOP_LEFT,
  WINDIV_PRINT_ON_TOP_CENTER,
  WINDIV_PRINT_ON_TOP_RIGHT,

  WINDIV_PRINT_ON_MIDDLE_LEFT,
  WINDIV_PRINT_ON_CENTER,
  WINDIV_PRINT_ON_MIDDLE_RIGHT,

  WINDIV_PRINT_ON_BOTTOM_LEFT,
  WINDIV_PRINT_ON_BOTTOM_CENTER,
  WINDIV_PRINT_ON_BOTTOM_RIGHT
};

enum print_on_err {
  WINDIV_E_PRINT_ON_NONE,
  WINDIV_E_PRINT_ON_NULL,
  WINDIV_E_PRINT_ON_MESSAGE_TOO_BIG
};

enum printline_on_align {
  WINDIV_PRINTLINE_ON_LEFT,
  WINDIV_PRINTLINE_ON_CENTER,
  WINDIV_PRINTLINE_ON_RIGHT
};

#define WINDIV_PADDINGS_SIZE_DEF 1
#define WINDIV_SIZE_PARENT      -1
#define WINDIV_PARENT_END     -1

/* Inicializa os dados da WinDiv com os valores padrões. */
void windiv_init(WinDiv *wd);

/* Faz o mesmo que windiv_init() e cria uma WINDOW relativa a 'parent'.
   Se <tamanho> é um número negativo, o <tamanho> final é equivalente a:
     <tamanho> de 'parent' - <tamanho> + 1
   Se <posição> é um número negativo, a <posição> final é equivalente a:
     <tamanho> de 'parent' - <posição>
   NOTA: os PADDINGS de 'parent' são ignorados. */
void windiv_create(WinDiv *wd, windiv_parent parent, int height, int width, int y, int x);

/* O mesmo que windiv_create(), mas com paddings especificáveis. */
void windiv_create_with_paddings(WinDiv *wd, windiv_parent parent, int height, int width, int y, int x,
				 int ptop, int pbottom, int pleft, int pright);

/* Apaga a Ncurses WINDOW e o buffer, caso existam. */
void windiv_finalize(WinDiv *wd);

/* Verifica se as coordenadas abolutas são relativas à WinDiv. */
#define windiv_checkyx(WD, ABS_Y, ABS_X)\
  (ABS_Y >= getbegy((WD).win) && ABS_Y < (getbegy((WD).win) + getmaxy((WD).win)) && \
   (ABS_X >= getbegx((WD).win) && ABS_X < (getbegx((WD).win) + getmaxx((WD).win))))

#define windiv_getheight(WD) ((WD).win ? getmaxy((WD).win) : 0)
#define windiv_getwidth(WD)  ((WD).win ? getmaxx((WD).win) : 0)
#define windiv_getabsy(WD)   ((WD).win ? getbegy((WD).win) : 0)
#define windiv_getabsx(WD)   ((WD).win ? getbegx((WD).win) : 0)
#define windiv_gety(WD)      ((WD).win ? gety((WD).win) : 0)
#define windiv_getx(WD)      ((WD).win ? getx((WD).win) : 0)

/* Se houve um clique em 'wd', retorna true ou callback(mevent, wd, arg) se
  callback não é um ponteiro nulo.
   Caso contrário, retorna false.
   Se wd é nulo, retorna se houve um clique na última verificação
  desta função.

   NOTA: para esta função funcionar, mousemask() deve ter sido configurado. */
void *wd_on_click(MEVENT *mev, WinDiv *wd, void *(*callback)(MEVENT*, WinDiv*, void*),
                  void *arg);

const char *windiv_printline_on(WinDiv *wd, printline_on_align on, int line,
			  const char *message);

/* Imprime uma mensagem na WinDiv com o alinhamento desejado. */
int windiv_print_on(WinDiv *wd, print_on_align on, const char *message);

/* Mesmo do anterior, mas com string de formato. */
int windiv_printf_on(WinDiv *wd, print_on_align on, const char *fmt, ...)
  __attribute__((format(printf, 3, 4)));

#define windiv_hide(WD) wclear((WD).win)
#define windiv_refresh(WD) wrefresh((WD).win)

#endif
