# Makefile
CC=clang
LINKER_REQS=-lncurses
DEPEND_GLIB_INC=`pkg-config glib-2.0 --cflags`
DEPEND_GLIB_LIB=`pkg-config glib-2.0 --libs`

all: all-o nplayer-bin

all-o: mk-build-dir nflog-o windiv-o nscreen-o nplayerbox-o nplayer-o
	# Nada além disso, vamos descansar por alguns segs
	sleep 3

run:
	# Executando nPlayer incondicionalmente
	build/nplayer

clean:
	# Removendo todos os binários de build
	rm build/*.o
	rm build/nplayer

mk-build-dir:
	mkdir -p build

nplayer-bin:
	$(CC) build/*.o -o build/nplayer $(LINKER_REQS) $(DEPEND_GLIB_LIB)

nflog-o:
	$(CC) nflog/nflog.c -c $(DEPEND_GLIB_INC)
	mv nflog.o build/

windiv-o:
	$(CC) windiv.c -c $(DEPEND_GLIB_INC)
	mv windiv.o build/

nscreen-o:
	$(CC) nscreen.c -c $(DEPEND_GLIB_INC)
	mv nscreen.o build/

nplayerbox-o:
	$(CC) nplayerbox.c -c $(DEPEND_GLIB_INC)
	mv nplayerbox.o build/

nplayer-o:
	$(CC) nplayer.c -c $(DEPEND_GLIB_INC)
	mv nplayer.o build/
