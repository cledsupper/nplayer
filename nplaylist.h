#ifndef _NPLAYLIST_H_
#define _NPLAYLIST_H_

/* Largura da playlist flutuante */
#define NPLAYLIST_FLOATING_WIDTH_PERCENT 0.45

struct _nplaylist {
  WinDiv wd;
  char *title;
};

/* ============= NSCREEN: PLAYLIST ============= */
//void nscreen_playlist_init();
//void nscreen_playlist_finalize();
//void nscreen_playlist_draw();

#endif
