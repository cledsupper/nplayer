#include <string.h>
#include <locale.h>

#include "nscreen.h"

#ifndef NDEBUG
#include <glib/gprintf.h>
void DEBUG_PRINT(const char *message, ...) {
  int y, x;
  va_list vlist;
  gchar *buffer;

  getmaxyx(stdscr, y, x);
  move(y-1, 1);
  for (int i=1; i < x-1; i++)
    addch(' ');
  move(y-1, 1);

  va_start(vlist, message);

  g_vasprintf(&buffer, message, vlist);
  addstr(buffer);
  va_end(vlist);

  fputs(buffer, nflog);
  fflush(nflog);
  g_free(buffer);

  move(0, 0);
  refresh();
  getch();
}
#endif

static void nscreen_windowbar_init();
static void nscreen_windowbar_finalize();
static void nscreen_windowbar_draw();

struct _nscreen nscreen;

bool nscreen_init() {
  WINDOW *win = initscr();
  if (!win)
	  return false;
  noecho();
  curs_set(CURSOR_OFF);
  mousemask(ALL_MOUSE_EVENTS, NULL);
  keypad(win, TRUE);
  //nodelay(win, TRUE);

  windiv_init(&(nscreen.wd));
  if (nscreen_configure(win)) {
    nscreen_windowbar_init();
    nscreen_playerbox_init();
    /*nscreen_playlist_init();
    nscreen_mainmenu_init();*/
    return true;
  }

  nodelay(stdscr, FALSE);
  return false;
}

static void nscreen_intern_finalize() {
  /*nscreen_mainmenu_finalize();
  nscreen_playlist_finalize();*/
  nscreen_playerbox_finalize();
  nscreen_windowbar_finalize();
}

void nscreen_finalize() {
  nscreen_intern_finalize();
  nscreen.wd.win = NULL;
  windiv_finalize(&(nscreen.wd));
  endwin();
}


bool nscreen_configure(WINDOW *parent) {
  nscreen.wd.win = parent;
  wclear(parent);
  wrefresh(parent);

  if (nscreen_rescale() == NSCREEN_SCALE_OUT) {
    windiv_printf_on(&(nscreen.wd),
		     WINDIV_PRINT_ON_BOTTOM_LEFT,
		     "Erro: o tamanho do terminal deve ser maior que %d x %d",
		     NSCREEN_WIDTH_MINIMAL, NSCREEN_HEIGHT_MINIMAL);
    return false;
  }
  return true;
}

void nscreen_draw_all(WINDOW *parent) {
  if (nscreen_configure(parent)) {
    nscreen_windowbar_draw();
    nscreen_playerbox_draw_all();
    /*nscreen_playlist_draw();
    nscreen_mainmenu_draw();*/
  }
}

NScreenScale nscreen_rescale() {
  NScreenScale scaling = NSCREEN_SCALE_LARGE;

  if (windiv_getheight(nscreen.wd) < NSCREEN_HEIGHT_LARGE) {
    if (windiv_getheight(nscreen.wd) >= NSCREEN_HEIGHT_NORMAL)
      scaling = NSCREEN_SCALE_NORMAL;
    else if (windiv_getheight(nscreen.wd) >= NSCREEN_HEIGHT_MINIMAL)
      scaling = NSCREEN_SCALE_MINIMAL;
    else {
      scaling = NSCREEN_SCALE_OUT;
      nscreen.scaling = scaling;
      return scaling;
    }
  }

  if (windiv_getwidth(nscreen.wd) < NSCREEN_WIDTH_LARGE) {
    if (windiv_getwidth(nscreen.wd) >= NSCREEN_WIDTH_NORMAL) {
      if (scaling > NSCREEN_SCALE_NORMAL)
	scaling = NSCREEN_SCALE_NORMAL;
      nscreen.scaling = scaling;
      return scaling;
    }
    if (windiv_getwidth(nscreen.wd) >= NSCREEN_WIDTH_MINIMAL) {
      if (scaling > NSCREEN_SCALE_MINIMAL)
	scaling = NSCREEN_SCALE_MINIMAL;
      nscreen.scaling = scaling;
      return scaling;
    }
    nscreen.scaling = NSCREEN_SCALE_OUT;
    return nscreen.scaling;
  }

  nscreen.scaling = scaling;
  return scaling;
}

static void nscreen_windowbar_init() {
  windiv_init(&(nscreen.windowbar.wd));
  nscreen_windowbar_title_set(WINDOWBAR_TITLE_STR_DEF);
  nscreen_windowbar_menu_set(WINDOWBAR_MENU_STR_DEF);
  nscreen_windowbar_draw();
}

static void nscreen_windowbar_finalize() {
  windiv_finalize(&(nscreen.windowbar.title.wd));
  windiv_finalize(&(nscreen.windowbar.menu.wd));
  windiv_finalize(&(nscreen.windowbar.wd));
}

static void nscreen_windowbar_draw() {
  if (nscreen.windowbar.wd.win) {
    windiv_finalize(&(nscreen.windowbar.title.wd));
    windiv_finalize(&(nscreen.windowbar.menu.wd));
    windiv_finalize(&(nscreen.windowbar.wd));
  }

  switch (nscreen.scaling) {
  /* ESCALA DE TELA COMPRIMIDA (MÍNIMA) */
  case NSCREEN_SCALE_MINIMAL:
    /* WINDOW da WindowBar */
    windiv_create_with_paddings(&(nscreen.windowbar.wd), (windiv_parent) &(nscreen.wd),
      WINDOWBAR_HEIGHT_MINIMAL, -1,
      0, 0,
      0, 0, 0, 0);

    /* WINDOW do 'menu' */
    windiv_create_with_paddings(&(nscreen.windowbar.menu.wd), (windiv_parent) &(nscreen.windowbar.wd),
      -1, WINDOWBAR_MENU_WIDTH_MINIMAL,
      0, - WINDOWBAR_MENU_WIDTH_MINIMAL - nscreen.windowbar.wd.padding.right,
      0, 0, 1, 1);

    /* Inicialização da WinDiv 'title' (o título da barra superior) */
    windiv_create_with_paddings(&(nscreen.windowbar.title.wd), (windiv_parent) &(nscreen.windowbar.wd),
      -1, - 1 - WINDOWBAR_MENU_WIDTH_MINIMAL,
      nscreen.windowbar.wd.padding.top, nscreen.windowbar.wd.padding.left,
      0, 0, 1, 1);
    break;

  /* ESCALA DE TELA NORMAL */
  case NSCREEN_SCALE_NORMAL:
  case NSCREEN_SCALE_LARGE:
    /* WINDOW da WindowBar */
    windiv_create(&(nscreen.windowbar.wd), (windiv_parent) &(nscreen.wd),
      WINDOWBAR_HEIGHT_NORMAL, -1, 0, 0);

    /* WINDOW do 'menu' */
    windiv_create_with_paddings(&(nscreen.windowbar.menu.wd), (windiv_parent) &(nscreen.windowbar.wd),
      -1, WINDOWBAR_MENU_WIDTH_NORMAL,
      0, - WINDOWBAR_MENU_WIDTH_NORMAL - nscreen.windowbar.wd.padding.right,
      1, 1, 1, 1);

    /* WINDOW de 'title' */
    windiv_create_with_paddings(&(nscreen.windowbar.title.wd), (windiv_parent) &(nscreen.windowbar.wd),
      -1, - 1 - windiv_getwidth(nscreen.windowbar.menu.wd),
      0, nscreen.windowbar.wd.padding.left,
      1, 1, 1, 1);
    break;

  case NSCREEN_SCALE_OUT:
    return;
  }

  nscreen_windowbar_refresh_all();
}

void nscreen_windowbar_title_set(const char *title) {
  g_strlcpy(nscreen.windowbar.title.s, title, WINDOWBAR_TITLE_STR_SIZE);
}

void nscreen_windowbar_title_sub_set(const char *title, const char *subtitle) {
  g_snprintf(nscreen.windowbar.title.s, WINDOWBAR_TITLE_STR_SIZE, "%s - %s", title, subtitle);
}

void nscreen_windowbar_subtitle_set(const char *subtitle) {
  g_snprintf(nscreen.windowbar.title.s, WINDOWBAR_TITLE_STR_SIZE, "%s - %s", WINDOWBAR_TITLE_STR_DEF, subtitle);
}

void nscreen_windowbar_menu_set(const char *menu) {
  g_snprintf(nscreen.windowbar.menu.s, WINDOWBAR_MENU_STR_SIZE, "%s", menu);
}

void nscreen_windowbar_refresh_all() {
  switch (nscreen.scaling) {
  case NSCREEN_SCALE_NORMAL:
  case NSCREEN_SCALE_LARGE:
    wborder(nscreen.windowbar.wd.win, ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LTEE, ACS_RTEE);
    wrefresh(nscreen.windowbar.wd.win);
    break;
  default:
    return;
  }
  nscreen_windowbar_title_refresh();
  nscreen_windowbar_menu_refresh();
}

void nscreen_windowbar_title_refresh() {
  switch (nscreen.scaling) {
  case NSCREEN_SCALE_MINIMAL:
    windiv_hide(nscreen.windowbar.title.wd);
    break;
  case NSCREEN_SCALE_NORMAL:
    windiv_hide(nscreen.windowbar.title.wd);
  case NSCREEN_SCALE_LARGE:
    wborder(nscreen.windowbar.title.wd.win, ' ', ' ', ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE);
    break;
  default:
    return;
  }
  wattron(nscreen.windowbar.title.wd.win, A_BOLD);
  windiv_printline_on(&(nscreen.windowbar.title.wd), WINDIV_PRINTLINE_ON_CENTER, nscreen.windowbar.title.wd.padding.top, nscreen.windowbar.title.s);
  wattroff(nscreen.windowbar.title.wd.win, A_BOLD);
  windiv_refresh(nscreen.windowbar.title.wd);
}

void nscreen_windowbar_menu_refresh() {
  char s[WINDOWBAR_MENU_STR_SIZE];
  int end;
  if (nscreen.scaling == NSCREEN_SCALE_OUT)
    return;

  windiv_hide(nscreen.windowbar.menu.wd);
  if (nscreen.scaling == NSCREEN_SCALE_MINIMAL) {
    end = strlen(nscreen.windowbar.menu.s);
    s[0] = nscreen.windowbar.menu.s[end-1];
    s[1] = 0;
  }
  else {
    strcpy(s, nscreen.windowbar.menu.s);
    wborder(nscreen.windowbar.menu.wd.win, ' ', ' ', ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE);
  }

  //wattron(nscreen.windowbar.menu.wd.win, A_STANDOUT);
  windiv_printline_on(&(nscreen.windowbar.menu.wd), WINDIV_PRINTLINE_ON_RIGHT, nscreen.windowbar.menu.wd.padding.top, s);
  //wattroff(nscreen.windowbar.menu.wd.win, A_STANDOUT);
  windiv_refresh(nscreen.windowbar.menu.wd);
}
