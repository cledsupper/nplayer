#include <stdarg.h>
#include "windiv.h"
#include <string.h>

static const char *windiv_type_str = "WinDiv";

void windiv_init(WinDiv *wd) {
  memset(wd, 0, sizeof(WinDiv));
  wd->__struct_type = windiv_type_str;
  wd->padding.top = wd->padding.bottom =
    wd->padding.left = wd->padding.right = WINDIV_PADDINGS_SIZE_DEF;
}

void windiv_create(WinDiv *wd, windiv_parent parent, int height, int width, int y, int x) {
  WINDOW *pwin;
  if (parent.windiv->__struct_type == windiv_type_str)
    pwin = parent.windiv->win;
  else
    pwin = parent.window;

  memset(wd, 0, sizeof(WinDiv));
  wd->__struct_type = windiv_type_str;

  wd->padding.top = wd->padding.bottom =
    wd->padding.left = wd->padding.right = WINDIV_PADDINGS_SIZE_DEF;

  int pmaxy = getmaxy(pwin), pmaxx = getmaxx(pwin);
  if (height < 0)
    height = pmaxy + height + 1;
  if (width < 0)
    width = pmaxx + width + 1;
  if (y < 0) y = pmaxy + y;
  if (x < 0) x = pmaxx + x;

  wd->win = derwin(pwin, height, width, y, x);
  wrefresh(wd->win);
}

void windiv_create_with_paddings(WinDiv *wd, windiv_parent parent, int height, int width, int y, int x,
				 int ptop, int pbottom,
				 int pleft, int pright) {
  WINDOW *pwin;
  if (parent.windiv->__struct_type == windiv_type_str) {
    pwin = parent.windiv->win;
  } else
    pwin = parent.window;

  memset(wd, 0, sizeof(WinDiv));
  wd->__struct_type = windiv_type_str;

  wd->padding.top = ptop;
  wd->padding.bottom = pbottom;
  wd->padding.left = pleft;
  wd->padding.right = pright;

  int pmaxy = getmaxy(pwin), pmaxx = getmaxx(pwin);
  if (height < 0)
    height = pmaxy + height + 1;
  if (width < 0)
    width = pmaxx + width + 1;
  if (y < 0) y = pmaxy + y;
  if (x < 0) x = pmaxx + x;

  wd->win = derwin(pwin, height, width, y, x);
  wrefresh(wd->win);
}

void windiv_finalize(WinDiv *wd) {
  if (wd->win) {
    delwin(wd->win);
    wd->win = NULL;
  }
}

void *wd_on_click(MEVENT *mev, WinDiv *wd, void *(*callback)(MEVENT*, WinDiv*, void*),
                  void *arg) {
  static bool clicked = false;
  bool _yes = clicked;
  clicked = false;
  if (!wd)
    return (void*)_yes;

  if ((mev->bstate & BUTTON1_CLICKED) &&
      windiv_checkyx(*wd, mev->y, mev->x)) {
    clicked = true;
    if (callback) return callback(mev, wd, arg);
  }
  else clicked = false;
  return (void*)clicked;
}

const char *windiv_printline_on(WinDiv *wd, printline_on_align on, int line, const char *message) {
  int x;
  int inner_x = windiv_getwidth(*wd) - (wd->padding.left + wd->padding.right);
  int line_len = 0;
  do {
    int wd_height = windiv_getheight(*wd);
    if (wd_height + line < 0 || line >= wd_height) {
      nflog_printf_err("[E] windiv_printline_on(): requested <line> (%d) is less or higher than windiv_height() (%d)\n", line, wd_height);
      return NULL;
    }
    else if (line < 0)
      line = wd_height + line;
  } while(false);

  for (; message[line_len] && message[line_len] != '\n'; line_len++)
    if (line_len >= inner_x) break;

  switch (on) {
  case WINDIV_PRINTLINE_ON_LEFT:
    wmove(wd->win, line, wd->padding.left);
    break;

  case WINDIV_PRINTLINE_ON_CENTER:
    wmove(wd->win, line, (windiv_getwidth(*wd)-line_len)/2);
    break;

  case WINDIV_PRINTLINE_ON_RIGHT:
    wmove(wd->win, line, windiv_getwidth(*wd) - wd->padding.right - line_len);
    break;
  }

  for (x = 0; x < inner_x && (message[x] && message[x] != '\n'); x++)
    waddch(wd->win, message[x]);
  return message+x;
}

// TODO: implementar este código corretamente!
int windiv_print_on(WinDiv *wd, print_on_align on, const char *message) {
  int inner_y = windiv_getheight(*wd) - (wd->padding.top + wd->padding.bottom);
  int inner_x = windiv_getwidth(*wd) - (wd->padding.left + wd->padding.right);

  if (!wd)
    return WINDIV_E_PRINT_ON_NULL;
  if (wd->win == NULL)
    return WINDIV_E_PRINT_ON_NULL;

  const char *cur = message;
  int line, y;
  switch (on) {
  case WINDIV_PRINT_ON_TOP_LEFT:
    for (y = wd->padding.top; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_LEFT, y, cur);
      if (*cur == '\n')
	cur++;
      if (*cur == 0)
	break;
    }
    break;

  case WINDIV_PRINT_ON_TOP_CENTER:
    for (y = wd->padding.top; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_CENTER, y, cur);
      if (*cur == '\n')
	cur++;
      if (*cur == 0)
	break;
    }
    break;
  case WINDIV_PRINT_ON_TOP_RIGHT:
    for (y = wd->padding.top; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_RIGHT, y, cur);
      if (*cur == '\n')
	cur++;
      if (*cur == 0)
	break;
    }
    break;

  default:
    break;
  }

  if (*cur == 0) {
    wrefresh(wd->win);
    return WINDIV_E_PRINT_ON_NONE;
  }


  /* We'll need to know lines to fit the text correctly */
  int lines=0;
  do {
    int c, lc;
    for (c=0, lc=0; message[c] && lines < inner_y; c++, lc++) {
      if (message[c] == '\n') {
	lc = 0;
        lines++;
      }
      if (lc > inner_x) {
	lc=0;
	lines++;
      }
    }
    if (message[c-1] != '\n' && lines < inner_y) lines++;
  } while(0);

  switch (on) {
  case WINDIV_PRINT_ON_MIDDLE_LEFT:
    for (y = (windiv_getheight(*wd)-lines)/2; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_LEFT, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  case WINDIV_PRINT_ON_CENTER:
    for (y = (windiv_getheight(*wd)-lines)/2; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_CENTER, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  case WINDIV_PRINT_ON_MIDDLE_RIGHT:
    for (y = (windiv_getheight(*wd)-lines)/2; y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_RIGHT, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  case WINDIV_PRINT_ON_BOTTOM_LEFT:
    for (y = windiv_getheight(*wd) - lines - wd->padding.bottom;
	 y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_LEFT, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  case WINDIV_PRINT_ON_BOTTOM_CENTER:
    for (y = windiv_getheight(*wd)-lines-wd->padding.bottom;
	 y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_CENTER, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  case WINDIV_PRINT_ON_BOTTOM_RIGHT:
    for (y = windiv_getheight(*wd)-lines-wd->padding.bottom;
	 y < windiv_getheight(*wd) - wd->padding.bottom; y++) {
      cur = windiv_printline_on(wd, WINDIV_PRINTLINE_ON_RIGHT, y, cur);
      if (*cur == '\n')
        cur++;
      if (*cur == 0)
        break;
    }
    break;

  default:
    break;
  }

  wrefresh(wd->win);
  if (*cur == 0)
    return WINDIV_E_PRINT_ON_NONE;
  return WINDIV_E_PRINT_ON_MESSAGE_TOO_BIG;
}


int windiv_printf_on(WinDiv *wd, print_on_align on, const char *fmt, ...) {
  va_list vlist;

  va_start(vlist, fmt);
  gchar *buffer = g_strdup_vprintf(fmt, vlist);
  va_end(vlist);
  int ret_print_on = windiv_print_on(wd, on, buffer);
  g_free(buffer);
  return ret_print_on;
}
