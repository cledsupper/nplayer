#ifndef _NPLAYERBOX_H_
#define _NPLAYERBOX_H_

/* Tamanhos do nPlayerBox */
//#define NPLAYERBOX_WIDTH_MINIMAL NSCREEN_WIDTH_MINIMAL
/* A altura será de acordo com o tamanho da nScreen, então isto é desnecessário. */
//#define NPLAYERBOX_HEIGHT_MINIMAL 5


/* Tamanhos do nPlayer Music Tags (MTags) */
#define NPB_MTAGS_HEIGHT_MINIMAL 3
#define NPB_MTAGS_HEIGHT_LARGE 5

/* Não é necessário pois a largura depende de c2 */
//#define NPB_MTAGS_C1_WIDTH_LARGE 40
/* A largura da coluna 2 do mtags (coluna de num música e ano de lançamento) é fixa e tem largura 6 */
#define NPB_MTAGS_C2_WIDTH_MINIMAL 6

/* Todas as colunas tem altura 2 */
#define NPB_MTAGS__HEIGHT_MINIMAL 2

/* Elementos do Music Tags e seus tamanhos */
#define NPB_MTAGS_ARTIST_STR_DEF N_STR_PLAYER_NAME

#define NPB_MTAGS_C1_MUSIC_STR_DEF N_STR_VERSION_STR
#define NPB_MTAGS_C1_ALBUM_STR_DEF "Todos os direitos reservados"

#define NPB_MTAGS_C2_TRACKN_INT_DEF N_INT_VERSION_REL
#define NPB_MTAGS_C2_TRACKN_STR_SIZE 4
#define NPB_MTAGS_C2_YEAR_INT_DEF 2019
#define NPB_MTAGS_C2_YEAR_STR_SIZE 5


/* Tamanhos do nPlayer Info (info) */
#define NPB_INFO_HEIGHT_MINIMAL 1
#define NPB_INFO_HEIGHT_LARGE 2

/* Tamanhos do nPlayer PlayerControls (playercontrols)*/
#define NPB_PLAYERCONTROLS_HEIGHT_MINIMAL 1
#define NPB_PLAYERCONTROLS_HEIGHT_NORMAL  2

#define NPB_PC_TIME_LABELS_HEIGHT_MINIMAL 1
#define NPB_PC_TIME_LABELS_WIDTH_MINIMAL 22
#define NPB_PC_TIME_LABELS_WIDTH_NORMAL 16

#define NPB_PC_TIME_HSCALE_HEIGHT_MINIMAL 1
#define NPB_PC_TIME_HSCALE_WIDTH_MINIMAL 8

#define NPB_PC_NPBUTTONS_HEIGHT_MINIMAL 1
#define NPB_PC_NPBUTTONS_WIDTH_MINIMAL 12

/* Ponto do HScale */
#define NPB_PC_TIME_POINT_CHAR_DEF 'v'

/* Labels do nPlayer Buttons (npbuttons) */
#define NPB_PC_NPBUTTONS__STR_UNTOUCHABLE "--"

#define NPB_PC_NPBUTTONS_BACK_STR_DEF     "<<"

#define NPB_PC_NPBUTTONS_PLAY_STR_PLAY    "nP"
#define NPB_PC_NPBUTTONS_PLAY_STR_PAUSE   "||"
#define NPB_PC_NPBUTTONS_PLAY_STR_RESUME  "|>"

#define NPB_PC_NPBUTTONS_PLAY_STR_DEF NPB_PC_NPBUTTONS_PLAY_STR_PLAY

#define NPB_PC_NPBUTTONS_NEXT_STR_DEF     ">>"

#define NPB_PC_NPBUTTONS__STR_SIZE 3

#define NPB_PC_TIME_MAX_DEF 0
#define NPB_PC_VOLUME_MAX_DEF 20

typedef struct _nplayerbox nPlayerBox;

struct _nplayerbox {
  WinDiv wd;

  struct _mtags { 
    WinDiv wd;
    const char *artist; // tamanhos variam

    /* Coluna 1 (esq): nome da música e nome do álbum */
    struct _c1 {
      WinDiv wd;
      const char *music;
      const char *album;
    } c1;

    /* Coluna 2 (dir): número da faixa e ano de lançamento do álbum */
    struct _c2 {
      WinDiv wd;
      char trackn[NPB_MTAGS_C2_TRACKN_STR_SIZE];
      char year[NPB_MTAGS_C2_YEAR_STR_SIZE];
    } c2;

  } mtags;

  /* Informações e letra */
  struct _playerinfo {
    WinDiv wd;
    const char *info;
  } info;

  struct _playercontrols {
    WinDiv wd;

    struct _musictimes { /* -o---- / 00:35        04:20 */
      WinDiv wd_labels;
      WinDiv wd_hscale;
    } time;

    struct _npbuttons { /* << nP >> */
      WinDiv wd;
      const char *prev;
      const char *nplay;
      const char *next;
    } npbuttons;

    struct _musicvolume {
      WinDiv wd;
      int current;
      int maximum;
    } volume;
  } playercontrols;
};

/* ======================  PLAYERBOX  ====================== */
void nscreen_playerbox_init();
void nscreen_playerbox_finalize();
void nscreen_playerbox_draw_all();

/* ====  PLAYERBOX's Music Tags (mtags) public methods  ==== */
void npb_mtags_artist_set(const char *name);
void npb_mtags_c1__set(const char *music, const char *album);
void npb_mtags_c2__set(int trackn, int year);

void npb_mtags_artist_refresh();
void npb_mtags_c1__refresh();
void npb_mtags_c2__refresh();

/* ===== PLAYERBOX's Player Info (npb_info) public methods ===== */
void npb_info_set(const char *info);
void npb_info_refresh();

 
/* ===  Player Controls (PlayerControls) public methods  === */
/* Os parâmetros para esta função devem ser inteiros.
 * A unidade de tempo é a mesma de time_t.
 * 'point_c' é o caractere que indicará a posição atual no hscale. */
void npb_pc_time_refresh(int cur, int max, char point_c);
void npb_pc_npbuttons__refresh();
void npb_pc_volume_refresh();

void npb_pc_npbuttons__set(const char *prev, const char *nplay, const char *next);
void npb_pc_volume_restart(int maximum);
void npb_pc_volume_set(int current);

#endif
