# nPlayer
Um reprodutor de músicas para console com a mesma facilidade de um player feito com uma interface gráfica.

A ideia deste reprodutor de músicas é que ele possa ser executado em pelo menos qualquer sistema baseado em Linux moderno, mas principalmente GNU/Linux e Android (especialmente pela distribuição Termux).

Abaixo apresento uma previsão da tela normal do nPlayer quando ele estiver pronto:
```
+-------------------------------------------------+
| nPlayer - "Criadora da Música - W        Menu M |
+-------------------------------------------------+
| (O nome de um bom Dj)                           |
|  Criadora da Música - Weekend                11 |
| (E o nome de um bom álbum desse Dj)        2018 |
|                                                 |
|                i'll pick you up                 |
|  ---------o-----------------------------------  |
| 00m50    04m35    <<  ||  >>         - -----> + |
+-------------------------------------------------+
```

Todos os menus e controles do nPlayer poderão ser selecionados com um clique ou toque na tela do terminal (se este suportar) e por teclas de atalho, que dependem do contexto (menus visíveis) para serem acionados.

IMPLICAÇÕES E REQUISIT0S DESEJÁVEIS:
- Exibir dicas ao usuário quando em "tédio":
- - a cada 5 segundos sem interação do usuário;
- - o usuário tiver pressionado teclas desconhecidas pelo reprodutor.
- A abertura de música deve ser feita por uma interface amigável ao usuário leigo, preferencialmente com a possibilidade de selecionar múltiplas músicas.
- Ao abrir músicas pelo menu, reproduzir a primeira automaticamente se a fila estava vazia antes de adicioná-las.

ALSO: ele PODERÁ suportar arquivos de legendas no futuro, ou mesmo baixar as letras da internet e o usuário gerá-las sozinho quando apertar um botão para adiantar os "versos".

by ledso
