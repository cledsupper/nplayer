#ifndef _NSCREEN_H_
#define _NSCREEN_H_

#include "windiv.h"
#include <panel.h>
#include "nplayerbox.h"
#include "nplaylist.h"
#include "nmenu.h"

#ifndef NDEBUG
void DEBUG_PRINT(const char *message, ...)
  __attribute__((format(printf, 1, 2)));
#else
#define DEBUG_PRINT(X)
#endif


/* ============= NSCREEN ============= */
// Em C++, enumerações e structs não precisam de typedef
#ifndef __cplusplus
typedef enum NScreenScale NScreenScale;
#endif

enum NScreenScale {
  NSCREEN_SCALE_OUT,
  NSCREEN_SCALE_MINIMAL,
  NSCREEN_SCALE_NORMAL,
  NSCREEN_SCALE_LARGE
};

enum mouse_cursor_set {
  CURSOR_OFF,
  CURSOR_ON,
  CURSOR_HIGHLY_VISIBLE
};

/* VERSIONAMENTO */
#define N_INT_VERSION_REL  0
/* Quando o programa estiver reproduzindo, pausando e navegando por músicas, ele terá alcançado a versão 0.1. */
#define N_INT_VERSION_SUB  0
#define N_INT_VERSION_BLD  422
//#define N_STR_VERSION_NAME "Versao"
#define N_STR_VERSION_REL #N_INT_VERSION_REL
#define N_STR_VERSION_SUB #N_INT_VERSION_SUB
#define N_STR_VERSION_BLD #N_INT_VERSION_BLD
#define N_STR_VERSION_STR "Versao 0.0.505"

/* Você, caro/a explorador, ainda entenderá o quanto este código é horrível :(
   Estas são strings que podem ser exibidas ou usadas em diversos lugares do
  nPlayer. */

// Os diretórios do nPlayer terão este nome
#define N_STR_APPNAME "nplayer"
// Para a barra de título
#define N_STR_PLAYER_NAME  "nPlayer"
// Nada em reprodução '-'
#define N_STR_PLAYING_NONE "nada em reproducao"

/* Tamanho em bytes de rótulos da WindowBar */
#define WINDOWBAR_TITLE_STR_SIZE 100
#define WINDOWBAR_MENU_STR_SIZE 15

struct _nscreen {
  WinDiv wd;
  NScreenScale scaling;

  struct _windowbar {
    WinDiv wd;

    struct _title {
      WinDiv wd;
      char s[WINDOWBAR_TITLE_STR_SIZE];
    } title;

    struct _menu {
      WinDiv wd;
      char s[WINDOWBAR_MENU_STR_SIZE];
    } menu;

  } windowbar;

  struct _overlapping_windows {
    nPlayerBox playerbox;

    struct _nplaylist *playlist;

    struct _nmenu *menu;

    PANEL *panels[3];
  } windows;
};

extern struct _nscreen nscreen;

/* Tamanhos do NScreen */
#define NSCREEN_HEIGHT_MINIMAL 7
#define NSCREEN_HEIGHT_NORMAL 11
#define NSCREEN_HEIGHT_LARGE 13

#define NSCREEN_WIDTH_MINIMAL 46
#define NSCREEN_WIDTH_NORMAL 51
#define NSCREEN_WIDTH_LARGE 73

/* Inicializa a tela do nPlayer.
   Possivelmente retornando falso se a escala da tela for menor que a mínima prevista. */
bool nscreen_init();

/* Finaliza a tela do nPlayer */
void nscreen_finalize();

/* Retorna a escala da tela atual. */
NScreenScale nscreen_rescale();

/* Prepara a tela do nPlayer, tamanhos e escala.
   Retorna <falso> se a escala é menor que a mínima. */
bool nscreen_configure(WINDOW *parent);

/* Redesenha a tela do nPlayer e todos os elementos */
void nscreen_draw_all(WINDOW *parent);


/* ============= NSCREEN: WINDOWBAR ============= */
#define WINDOWBAR_HEIGHT_MINIMAL 2
#define WINDOWBAR_HEIGHT_NORMAL 3

/* WindowBar elements defaults */
#define WINDOWBAR_TITLE_STR_DEF N_STR_PLAYER_NAME
#define WINDOWBAR_MENU_STR_BACK "Voltar X"
#define WINDOWBAR_MENU_STR_CLOSE "Fechar X"
#define WINDOWBAR_MENU_STR_MENU "Menu M"
#define WINDOWBAR_MENU_STR_SAVE "Salvar S"
#define WINDOWBAR_MENU_STR_DEF WINDOWBAR_MENU_STR_CLOSE

/* O indicador de menu mostrará essas dicas quando em 'idle' */
static const char *WINDOWBAR_MENU_STR_TIPS[] = {
  "Retroceder b",
  "Reproduzir n",
  "Pausar n",
  "Avancar m",
  "Abrir mus a",
  "Remover <DEL>",
  "Sel cursor v",
  "Sel volume v",
};

#define WINDOWBAR__HEIGHT_MINIMAL 1
#define WINDOWBAR__HEIGHT_NORMAL 3

#define WINDOWBAR_TITLE_WIDTH_MINIMAL 43

#define WINDOWBAR_MENU_WIDTH_MINIMAL 3
#define WINDOWBAR_MENU_WIDTH_NORMAL (2+WINDOWBAR_MENU_STR_SIZE-1)

void nscreen_windowbar_title_sub_set(const char *title, const char *subtitle);
void nscreen_windowbar_title_set(const char *title);
void nscreen_windowbar_subtitle_set(const char *subtitle);
void nscreen_windowbar_menu_set(const char *menu);

void nscreen_windowbar_refresh_all();
void nscreen_windowbar_title_refresh();
void nscreen_windowbar_menu_refresh();


#endif // _NSCREEN_H_
