/* nplayer.c
   Este é o código fonte do "back-end" do nPlayer, nplayer ou NPlayer.

   Curiosidade 1: o 'n' de n/QualquerCoisa não tem nada a ver com a biblioteca Ncurses, mas sim com o final do meu nome: cledso/n. Então o nome deste programa, por extenso, seria:
    =>  Player de Cledson.
   Mas você tem o direito de chamar e ressignificar o conceito do nPlayer como quiser – desde que não ofenda os termos da licença. Inclusive se você não gostou de saber desse fato, chame-o de "Not for dumbs Player", afinal ele não foi feito para idiotas, mas para Termuxers e aventureiros do terminal do Linux.

   Curiosidade 2: esse programa foi escrito com uso dos editores gedit, nano e emacs; foi bastante testado com o maravilhoso compilador Clang e o humilde, mas cheio de recursos, aplicativo Termux para Android; e por fim, ele ainda está aberto a mudanças sob a licença da MIT (exceto em arquivos em que estejam presentes avisos de licença, ou em diretórios um arquivo "LICENSE" ou "COPYING").

   Leia as notas de licença.
*/
#include <signal.h>
#include <stdlib.h>

#include "nscreen.h"

#include <unistd.h>

static void *npb_info_warn_clicked(MEVENT *mev, WinDiv *wd, void *arg) {
  static bool click_one = false;
  if (!click_one) {
    click_one = true;
    npb_info_set("você achou o lyrics '-'");
  } else {
    click_one = false;
    npb_info_set(N_STR_PLAYING_NONE);
  }
  npb_info_refresh();
  return (void*)true;
}


const char *init_error_arg = "--on-init-error";

static int on_init_error_retry(int argc, char *argv[]) {
	if (argc>1) {
		if (g_str_equal(argv[argc-1], init_error_arg))
			return 1;
	}
	GString *scmd = g_string_new("x-terminal-emulator -e ");
	GError *gerror = NULL;
	int i;
	for (i=0; i < argc; i++)
		g_string_append_printf(scmd, "%s ", argv[i]);
	g_string_append(scmd, init_error_arg);
	g_spawn_command_line_async(scmd->str, &gerror);
	if (gerror) {
		const char *retry_error_msg =
		  "Erro quando teimei em iniciar.\nMensagem de erro:\n%s\n";
		fprintf(nflog, retry_error_msg, gerror->message);
		g_error_free(gerror);
		g_string_free(scmd, TRUE);
		return 1;
	}
	g_string_free(scmd, TRUE);
	return 0;
}


static int n_main(int argc, char *argv[]);

int main(int argc, char *argv[]) {
  nflog_open(N_STR_APPNAME);
  atexit(nflog_close);

  if (argc > 1) {
    if (getopt(argc, argv, "w") != -1
      && !g_str_equal(argv[argc-1], init_error_arg))
      return on_init_error_retry(argc, argv);
  }
  if (isatty(0) == 0)
    return on_init_error_retry(argc, argv);

  atexit(nscreen_finalize);

  if (!nscreen_init())
    return on_init_error_retry(argc, argv);

  return n_main(argc, argv);
}

#ifndef __cplusplus
typedef enum NIdleTimeModes NIdleTimeModes;
#endif

enum NIdleTimeModes {
  N_IDLE_TIME_RARE,
  N_IDLE_TIME_HIGH,
  N_IDLE_TIME_LONG,
  N_IDLE_TIME_MODES
};

static const int N_IDLE_TIME[N_IDLE_TIME_MODES] = {
  1,
  10,
  600
};

static int n_main(int argc, char *argv[]) {
  struct _idle {
    NIdleTimeModes state;
    time_t last_state_change;
  };
  MEVENT mev;
  int c;
  time_t tcurrent = time(NULL);
  struct _idle idle = {N_IDLE_TIME_RARE, tcurrent};
  halfdelay(idle.state);

  while (g_unichar_toupper((c = getch())) != 'X') {
    tcurrent = time(NULL);

    if (c != ERR) {
      if (c == KEY_RESIZE)
	nscreen_draw_all(stdscr);
      else if (c == KEY_MOUSE && nscreen.scaling == NSCREEN_SCALE_LARGE) {
	if (getmouse(&mev) == OK) {
	  if (wd_on_click(&mev, &(nscreen.windowbar.menu.wd), NULL, NULL))
	    break;
	  else wd_on_click(&mev, &(nscreen.windows.playerbox.info.wd), npb_info_warn_clicked, NULL);
	}
      } else switch(c) {
	case 'a': case 'A':
	  break;

	case '<':
	case 'b': case 'B':
	  break;

	case 'n': case 'N':
	case KEY_ENTER:
	  break;

	case '>':
	case 'm':
	  break;
	}
      idle.last_state_change = tcurrent;
    }
    else {
      time_t count = tcurrent - idle.last_state_change;
      if (count > idle.state && idle.state < N_IDLE_TIME_LONG) {
      idle.state++;
      idle.last_state_change = tcurrent;
      }
    }
  }

  return 0;
}
