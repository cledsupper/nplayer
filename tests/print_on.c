#include <ncurses.h>
#include <windiv.h>

int main() {
  initscr();
  cbreak();
  noecho();
  refresh();

  WinDiv mini_wd;
  windiv_create(&mini_wd, (windiv_parent) stdscr,
  	4, 20, 1, 1); //← Cria a WinDiv
  // quero bordas razoáveis
  box(mini_wd.win, 0, 0);

  // um título
  wattron(mini_wd.win, A_STANDOUT);
  mvwprintw(mini_wd.win, 0, 1, "Mini Window");
  wattroff(mini_wd.win, A_STANDOUT);
  // e uma mensagem que espero ser razoável pro teste
  windiv_print_on(&(mini_wd),
		  WINDIV_PRINT_ON_TOP_RIGHT,
     /* eu não vou traduzir isso ↑ pra vc -_- */
		  "Grandes maltapralhsoajdd");
  // "pause"
  getch();

  WinDiv over_win;
  windiv_create(&over_win, (windiv_parent) stdscr,
    LINES-4, COLS-4,
    2, 2);
  box(over_win.win, 0, 0);
  wattron(over_win.win, A_STANDOUT);
  mvwprintw(over_win.win, 0, 1, "Over Window [any key to close]");
  wattroff(over_win.win, A_STANDOUT);
  windiv_print_on(&(over_win), WINDIV_PRINT_ON_CENTER,
  	"Estou no meio!!! Pressione X pra eu sair '-'");

  getch();
  wclear(over_win.win);
  wrefresh(over_win.win);
  windiv_finalize(&over_win);

  wclear(mini_wd.win);
  windiv_print_on(&(mini_wd),
  	WINDIV_PRINT_ON_CENTER,
  	"Enter para encerrar");
  getch();
  windiv_finalize(&mini_wd);
  endwin();
  return 0;
}
