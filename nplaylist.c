#include "nscreen.h"

static nPlayList *nplaylist = NULL;

void nscreen_playlist_init() {
  nplaylist = &(nscreen.windows.playlist);
  windiv_init(&(nplaylist.wd));
  nscreen_playlist_draw();
}

void nscreen_playlist_finalize() {
  windiv_finalize(&(nplaylist.wd));
}

void nscreen_playlist_draw() {
  if (nplaylist->wd.win)
    nscreen_playlist_finalize();

  if (nscreen.scaling != NSCREEN_SCALE_MINIMAL) {
    int wbar_height = windiv_getheight(nscreen.windowbar.wd);
    windiv_create(&(nplaylist->wd), (windiv_parent) &(nscreen.wd),
		  -1 - wbar_height, -1,
		  wbar_height, 0);
    nscreen_playlist_refresh();
  }
}

void nscreen_playlist_refresh() {
  windiv_printf_on(&(nplaylist->wd), WINDIV_PRINT_ON_CENTER, "nPlayList %dx%d", windiv_getwidth(nplaylist->wd), windiv_getheight(nplaylist->wd));
}
