#include "nflog.h"
#include <stdarg.h>
#include <time.h>
//#include <string.h>

FILE *nflog = NULL;
static int nflog_header_size;
static GMutex nflog_mutex;

int nflog_open(const char *app_name) {
  GString *slog = g_string_new(g_get_user_cache_dir());
  g_string_append_printf(slog, "%c%s%c",
			 G_DIR_SEPARATOR,
			 app_name,
			 G_DIR_SEPARATOR);
  if (!g_mkdir_with_parents(slog->str, 0700)) {
    g_string_append(slog, "n.log");
    nflog = fopen(slog->str, "a");
    if (nflog) {
      time_t now = time(NULL);
      struct tm *tm_now;
      tm_now = localtime(&now);
      char fmttime[20];
      strftime(fmttime, sizeof(fmttime), "%b %d, %G. %R", tm_now);
      nflog_header_size = fprintf(nflog, "%s==========  %s | %s ==========%s", N_NEWLINE_SEQ_STR,
				  app_name, fmttime, N_NEWLINE_SEQ_STR)
	- (sizeof(N_NEWLINE_SEQ_STR)-1)*2;
    }
  }
  g_string_free(slog, TRUE);
  // em caso de erros inesperados...
  // memset(&nflog_mutex, 0, sizeof(GMutex));
  return nflog != NULL ? 0 : -1;
}

int nflog_printf(const char *fmt, ...) {
  va_list vl;
  int ret = 0;

  va_start(vl, fmt);
  if (nflog) {
    g_mutex_lock(&nflog_mutex);
    ret = vfprintf(nflog, fmt, vl);
    g_mutex_unlock(&nflog_mutex);
  }
  va_end(vl);

  return ret;
}

int nflog_printf_err(const char *fmt, ...) {
  int errret = 0;
  va_list vl_stderr, vl_nflog;

  va_start(vl_stderr, fmt);
  va_copy(vl_nflog, vl_stderr);

  errret = vfprintf(stderr, fmt, vl_stderr);
  fflush(stderr);
  va_end(vl_stderr);

  if (nflog) {
    g_mutex_lock(&nflog_mutex);
    vfprintf(nflog, fmt, vl_nflog);
    fflush(nflog);
    g_mutex_unlock(&nflog_mutex);
  }
  va_end(vl_nflog);

  return errret;
}

void nflog_close() {
  if (nflog) {
    int h;
    for (h=0; h < nflog_header_size; h++)
      fputc('=', nflog);
    fputs(N_NEWLINE_SEQ_STR, nflog);
    fclose(nflog);
    nflog = NULL;
  }
}
