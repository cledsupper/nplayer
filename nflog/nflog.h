/* nflog.h - doc. e declarações do nflog.
   Esta biblioteca deve ser portátil e é entendida para funcionar como se fosse uma "saída padrão de relatório" de um programa nPlayer-like.  */
#ifndef _N_FLOG_H_
#define _N_FLOG_H_
#include <stdio.h>
#include <glib.h>

#ifdef G_OS_WIN32
#define N_NEWLINE_SEQ_STR "\r\n"
#else
#define N_NEWLINE_SEQ_STR "\n"
#endif

extern FILE *nflog;

/* Cria um n.log para o programa 'app_name'.
   Este arquivo estará presente em:
    $HOME/.cache/[app_name]/n.log */
int nflog_open(const char *app_name);

/* Escreve uma mensagem em n.log.
   Se nflog for NULL, retorna 0. Caso contrário, retorna vfprintf().

   NOTA: as funções nflog*printf() podem ser chamadas por mútiplas threads. */
int nflog_printf(const char *fmt, ...)
  __attribute__((format(printf, 1, 2)));

/* Escreve uma mensagem de erro na saída de erro padrão e escreve-a em n.log de modo imediato.
   Retorna a quantidade de bytes escritos na saída de erro padrão.

   NOTA: ao n.log é garantida a escrita por apenas uma thread. Se nflog for nulo, não escreve nada e retorna 0. */
int nflog_printf_err(const char *fmt, ...)
  __attribute__((format(printf, 1, 2)));

/* Fecha o arquivo n.log. */
void nflog_close();

#endif
