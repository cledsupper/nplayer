#include "nscreen.h"

#include <string.h>
#include <ctype.h>

static nPlayerBox *nplayerbox = NULL;

/* =========  PLAYERBOX private methods  ========= */
static void nscreen_playerbox_draw();

/* =====  PLAYERBOX's MTags private methods  ===== */
static void npb_mtags_init();
static void npb_mtags_finalize();
static void npb_mtags_draw();

/* ===========  PInfo private methods  ============ */
static void npb_info_init();
static void npb_info_finalize();
static void npb_info_draw();

/* =======  PlayerControls private methods  ======= */
static void npb_playercontrols_init();
static void npb_playercontrols_finalize();
static void npb_playercontrols_draw();
static void npb_playercontrols_draw_all();

static void npb_pc_time_init();
static void npb_pc_time_finalize();
static void npb_pc_time_draw_all();

static void npb_pc_npbuttons_init();
static void npb_pc_npbuttons_finalize();
static void npb_pc_npbuttons_draw();

static void npb_pc_volume_init();
static void npb_pc_volume_finalize();
static void npb_pc_volume_draw();

/* =============  nPlayerBox methods  ============= */

void nscreen_playerbox_init() {
  nplayerbox = &(nscreen.windows.playerbox);
  memset(nplayerbox, 0, sizeof(nPlayerBox));

  nscreen_playerbox_draw();

  npb_mtags_init();
  npb_info_init();
  npb_playercontrols_init();
}

void nscreen_playerbox_finalize() {
  if (nplayerbox) {
    npb_playercontrols_finalize();
    npb_info_finalize();
    npb_mtags_finalize();
    windiv_finalize(&(nplayerbox->wd));
  }
}

static void nscreen_playerbox_draw() {
  if (nplayerbox->wd.win)
    nscreen_playerbox_finalize();

  switch (nscreen.scaling) {
    case NSCREEN_SCALE_MINIMAL:
      windiv_create_with_paddings(&(nplayerbox->wd), (windiv_parent) &(nscreen.wd),
        - 1 - WINDOWBAR_HEIGHT_MINIMAL,
        -1,
        WINDOWBAR_HEIGHT_MINIMAL, 0,
        0, 0, 0, 0);
      break;

    case NSCREEN_SCALE_NORMAL:
    case NSCREEN_SCALE_LARGE:
      windiv_create_with_paddings(&(nplayerbox->wd), (windiv_parent) &(nscreen.wd),
        - 1 - WINDOWBAR_HEIGHT_NORMAL,
        -1,
        WINDOWBAR_HEIGHT_NORMAL, 0,
        0, 1, 1, 1);
      wborder(nplayerbox->wd.win, ACS_VLINE, ACS_VLINE, ' ', ACS_HLINE, ACS_VLINE, ACS_VLINE, ACS_LLCORNER, ACS_LRCORNER);
      wrefresh(nplayerbox->wd.win);
      break;

    case NSCREEN_SCALE_OUT:
      return;
  }
}

void nscreen_playerbox_draw_all() {
  nscreen_playerbox_draw();
  npb_mtags_draw();
  npb_info_draw();
  npb_playercontrols_draw_all();
}


/* ===============  MTags methods  =============== */
static void npb_mtags_init() {
  npb_mtags_artist_set(NPB_MTAGS_ARTIST_STR_DEF);
  npb_mtags_c1__set(NPB_MTAGS_C1_MUSIC_STR_DEF, NPB_MTAGS_C1_ALBUM_STR_DEF);
  npb_mtags_c2__set(NPB_MTAGS_C2_TRACKN_INT_DEF, NPB_MTAGS_C2_YEAR_INT_DEF);

  npb_mtags_draw();
}

static void npb_mtags_finalize() {
  windiv_finalize(&(nplayerbox->mtags.c2.wd));
  windiv_finalize(&(nplayerbox->mtags.c1.wd));
  windiv_finalize(&(nplayerbox->mtags.wd));
}

static void npb_mtags_draw() {
  if (nplayerbox->mtags.wd.win)
    npb_mtags_finalize();

  bool scale_out = false;

  switch (nscreen.scaling) {
  case NSCREEN_SCALE_MINIMAL:
  case NSCREEN_SCALE_NORMAL:
    windiv_create_with_paddings(&(nplayerbox->mtags.wd), (windiv_parent) &(nplayerbox->wd),
				NPB_MTAGS_HEIGHT_MINIMAL,
				windiv_getwidth(nplayerbox->wd)
				 - nplayerbox->wd.padding.left - nplayerbox->wd.padding.right,
				nplayerbox->wd.padding.top, nplayerbox->wd.padding.left,
				0, 0, 1, 1);
    break;

  case NSCREEN_SCALE_LARGE:
    /* O tamanho máximo do mtags é NPB_MTAGS_WIDTH_MINIMAL. Ele não deve crescer além disso. */
    windiv_create(&(nplayerbox->mtags.wd), (windiv_parent) &(nplayerbox->wd),
		  NPB_MTAGS_HEIGHT_LARGE,
		  - (windiv_getwidth(nplayerbox->wd)*NPLAYLIST_FLOATING_WIDTH_PERCENT),
		  nplayerbox->wd.padding.top, nplayerbox->wd.padding.left);
    break;

  case NSCREEN_SCALE_OUT:
    scale_out = true;
    break;
  }

  if (!scale_out) {
    /* Inicialização da c1 (coluna mtags 1 para nome de música e nome de álbum) */
    windiv_create_with_paddings(&(nplayerbox->mtags.c1.wd), (windiv_parent) &(nplayerbox->mtags.wd),
      NPB_MTAGS__HEIGHT_MINIMAL,
       - 1 - NPB_MTAGS_C2_WIDTH_MINIMAL,
      nplayerbox->mtags.wd.padding.top + 1, 0,
      0, 0, 1, 1);
    // as colunas do MTags sobrepõe os preenchimentos da tabela!

    /* Inicialização da c2 (coluna mtags 2 para número da faixa e ano do álbum) */
    windiv_create_with_paddings(&(nplayerbox->mtags.c2.wd), (windiv_parent) &(nplayerbox->mtags.wd),
      NPB_MTAGS__HEIGHT_MINIMAL,
      NPB_MTAGS_C2_WIDTH_MINIMAL,
      nplayerbox->mtags.wd.padding.top + 1, - NPB_MTAGS_C2_WIDTH_MINIMAL,
       0, 0, 1, 1);

    npb_mtags_artist_refresh();
    npb_mtags_c1__refresh();
    npb_mtags_c2__refresh();
  }
}

void npb_mtags_artist_set(const char *name) {
  nplayerbox->mtags.artist = name;
}

void npb_mtags_c1__set(const char *music, const char *album) {
  nplayerbox->mtags.c1.music = music;
  nplayerbox->mtags.c1.album = album;
}

void npb_mtags_c2__set(int trackn, int year) {
  g_snprintf(nplayerbox->mtags.c2.trackn, NPB_MTAGS_C2_TRACKN_STR_SIZE, "%d", trackn);
  g_snprintf(nplayerbox->mtags.c2.year, NPB_MTAGS_C2_YEAR_STR_SIZE, "%d", year);
}

void npb_mtags_artist_refresh() {
  windiv_printline_on(&(nplayerbox->mtags.wd), WINDIV_PRINTLINE_ON_LEFT, nplayerbox->mtags.wd.padding.top,  nplayerbox->mtags.artist);
  wrefresh(nplayerbox->mtags.wd.win);
}

void npb_mtags_c1__refresh() {
  wattron(nplayerbox->mtags.c1.wd.win, A_ITALIC);
  windiv_printline_on(&(nplayerbox->mtags.c1.wd), WINDIV_PRINTLINE_ON_LEFT, nplayerbox->mtags.c1.wd.padding.top,  nplayerbox->mtags.c1.music);
  wattroff(nplayerbox->mtags.c1.wd.win, A_ITALIC);
  windiv_printline_on(&(nplayerbox->mtags.c1.wd), WINDIV_PRINTLINE_ON_LEFT, nplayerbox->mtags.c1.wd.padding.top+1, nplayerbox->mtags.c1.album);

  wrefresh(nplayerbox->mtags.c1.wd.win);
}

void npb_mtags_c2__refresh() {
  windiv_printline_on(&(nplayerbox->mtags.c2.wd), WINDIV_PRINTLINE_ON_RIGHT, nplayerbox->mtags.c2.wd.padding.top,  nplayerbox->mtags.c2.trackn);
  windiv_printline_on(&(nplayerbox->mtags.c2.wd), WINDIV_PRINTLINE_ON_RIGHT, nplayerbox->mtags.c2.wd.padding.top+1,  nplayerbox->mtags.c2.year);
  wrefresh(nplayerbox->mtags.c2.wd.win);
}


/* ===============  NPInfo methods  =============== */
static void npb_info_init() {
  npb_info_set(N_STR_PLAYING_NONE);
  npb_info_draw();
}

static void npb_info_finalize() {
  windiv_finalize(&(nplayerbox->info.wd));
}

static void npb_info_draw() {
  if (nplayerbox->info.wd.win)
    npb_info_finalize();

  int minimal_y = 4;
  switch (nscreen.scaling) {
    case NSCREEN_SCALE_OUT:
      break;

    case NSCREEN_SCALE_MINIMAL:
      minimal_y = 2; // @suppress("No break at end of case")
    case NSCREEN_SCALE_NORMAL:
      windiv_create_with_paddings(&(nplayerbox->info.wd), (windiv_parent) &(nplayerbox->wd),
        NPB_INFO_HEIGHT_MINIMAL,
         - 1 - nplayerbox->wd.padding.left - nplayerbox->wd.padding.right,
         - minimal_y, nplayerbox->wd.padding.left,
         0, 0, 0, 0);

      npb_info_refresh();
      break;

    case NSCREEN_SCALE_LARGE:
      windiv_create_with_paddings(&(nplayerbox->info.wd), (windiv_parent) &(nplayerbox->wd),
        NPB_INFO_HEIGHT_LARGE,
        - 1 - (windiv_getwidth(nplayerbox->wd)*NPLAYLIST_FLOATING_WIDTH_PERCENT)
           - nplayerbox->wd.padding.left - nplayerbox->wd.padding.right,
        - minimal_y - nplayerbox->wd.padding.bottom, nplayerbox->wd.padding.left,
        0, 0, 0, 0);

      npb_info_refresh();
      break;
  }
}

void npb_info_set(const char *info) {
  nplayerbox->info.info = info;
}

void npb_info_refresh() {
  wclear(nplayerbox->info.wd.win);
  windiv_printline_on(&(nplayerbox->info.wd), WINDIV_PRINTLINE_ON_CENTER, nplayerbox->info.wd.padding.top, nplayerbox->info.info);
  wrefresh(nplayerbox->info.wd.win);
}


/* ========  NPB PlayerControls methods  ======== */
static void npb_playercontrols_init() {
  windiv_init(&(nplayerbox->playercontrols.wd));
  npb_playercontrols_draw();

  npb_pc_time_init();
  npb_pc_npbuttons_init();
  npb_pc_volume_init();
}

static void npb_playercontrols_finalize() {
  npb_pc_volume_finalize();
  npb_pc_npbuttons_finalize();
  npb_pc_time_finalize();

  windiv_finalize(&(nplayerbox->playercontrols.wd));
}

static void npb_playercontrols_draw() {
  if (nplayerbox->playercontrols.wd.win)
    npb_playercontrols_finalize();

  switch (nscreen.scaling) {
  case NSCREEN_SCALE_OUT:
    break;
  case NSCREEN_SCALE_MINIMAL:
    windiv_create_with_paddings(&(nplayerbox->playercontrols.wd), (windiv_parent) &(nplayerbox->wd),
      NPB_PLAYERCONTROLS_HEIGHT_MINIMAL,
      - 1 - nplayerbox->wd.padding.left - nplayerbox->wd.padding.right,
      -(NPB_PLAYERCONTROLS_HEIGHT_MINIMAL)
        - nplayerbox->wd.padding.bottom,
      nplayerbox->wd.padding.left,
      0, 0, 0, 0);
    break;

  case NSCREEN_SCALE_NORMAL:
  case NSCREEN_SCALE_LARGE:
    windiv_create_with_paddings(&(nplayerbox->playercontrols.wd), (windiv_parent) &(nplayerbox->wd),
      NPB_PLAYERCONTROLS_HEIGHT_NORMAL,
      - 1 - nplayerbox->wd.padding.left - nplayerbox->wd.padding.right,
      -(NPB_PLAYERCONTROLS_HEIGHT_NORMAL)
        - nplayerbox->wd.padding.bottom,
      nplayerbox->wd.padding.left,
      0, 0, 0, 0);
    break;
  }
}

static void npb_playercontrols_draw_all() {
  npb_playercontrols_draw();
  npb_pc_time_draw_all();
  npb_pc_npbuttons_draw();
  npb_pc_volume_draw();
}


/* ============  NPB PC Time methods  ============ */
static void npb_pc_time_init() {
  windiv_init(&(nplayerbox->playercontrols.time.wd_hscale));
  windiv_init(&(nplayerbox->playercontrols.time.wd_labels));

  npb_pc_time_draw_all();
}

static void npb_pc_time_finalize() {
  windiv_finalize(&(nplayerbox->playercontrols.time.wd_hscale));
  windiv_finalize(&(nplayerbox->playercontrols.time.wd_labels));
}

static void npb_pc_time_draw_all() {
  if (nplayerbox->playercontrols.time.wd_hscale.win)
	  npb_pc_time_finalize();

  switch (nscreen.scaling) {
  case NSCREEN_SCALE_OUT:
    break;

  case NSCREEN_SCALE_MINIMAL:
    windiv_create_with_paddings(&(nplayerbox->playercontrols.time.wd_labels), (windiv_parent) &(nplayerbox->playercontrols.wd),
    		NPB_PC_TIME_LABELS_HEIGHT_MINIMAL, NPB_PC_TIME_LABELS_WIDTH_MINIMAL,
			0, 0,
			0, 0, 1, 1);
    windiv_create_with_paddings(&(nplayerbox->playercontrols.time.wd_hscale), (windiv_parent) &(nplayerbox->playercontrols.time.wd_labels),
    		NPB_PC_TIME_HSCALE_HEIGHT_MINIMAL, NPB_PC_TIME_HSCALE_WIDTH_MINIMAL,
			0, (windiv_getwidth(nplayerbox->playercontrols.time.wd_labels)-NPB_PC_TIME_HSCALE_WIDTH_MINIMAL)/2,
			0, 0, 1, 1);
    break;

  case NSCREEN_SCALE_NORMAL:
  case NSCREEN_SCALE_LARGE:
    windiv_create_with_paddings(&(nplayerbox->playercontrols.time.wd_labels), (windiv_parent) &(nplayerbox->playercontrols.wd),
    		NPB_PC_TIME_LABELS_HEIGHT_MINIMAL, NPB_PC_TIME_LABELS_WIDTH_NORMAL,
			-1 - nplayerbox->playercontrols.wd.padding.bottom, nplayerbox->playercontrols.wd.padding.left,
			0, 0, 1, 1);
    windiv_create_with_paddings(&(nplayerbox->playercontrols.time.wd_hscale), (windiv_parent) &(nplayerbox->playercontrols.wd),
    		NPB_PC_TIME_HSCALE_HEIGHT_MINIMAL,
			- 1 - (nplayerbox->playercontrols.wd.padding.left + nplayerbox->playercontrols.wd.padding.right),
			nplayerbox->playercontrols.wd.padding.top, nplayerbox->playercontrols.wd.padding.left,
			0, 0, 1, 1);
    break;
  }

  npb_pc_time_refresh(0, 0, NPB_PC_TIME_POINT_CHAR_DEF);
}

void npb_pc_time_refresh(int cur, int max, char point_c) {
  /* Show label */
  windiv_printf_on(&(nplayerbox->playercontrols.time.wd_labels), WINDIV_PRINT_ON_TOP_LEFT, "%2d:%02d", cur/60, cur%60);
  windiv_printf_on(&(nplayerbox->playercontrols.time.wd_labels), WINDIV_PRINT_ON_TOP_RIGHT, "%2d:%02d", max/60, max%60);

  /* Draw horizontal scale */
  if (!isgraph(point_c) || point_c == '-')
	  point_c = 'v';

  float f_scale = (double)cur/max;
  int x_locale = (f_scale * (windiv_getwidth(nplayerbox->playercontrols.time.wd_hscale)-2)) + 1;
  wmove(nplayerbox->playercontrols.time.wd_hscale.win,
		  nplayerbox->playercontrols.time.wd_hscale.padding.top,
		  nplayerbox->playercontrols.time.wd_hscale.padding.right);
  int x;
  for (x = nplayerbox->playercontrols.time.wd_hscale.padding.left;
       x < windiv_getwidth(nplayerbox->playercontrols.time.wd_hscale)
        - nplayerbox->playercontrols.time.wd_hscale.padding.right; x++) {
	  if (x == x_locale)
		  waddch(nplayerbox->playercontrols.time.wd_hscale.win, point_c);
	  else
		  waddch(nplayerbox->playercontrols.time.wd_hscale.win, '-');
  }

  wrefresh(nplayerbox->playercontrols.time.wd_hscale.win);
}


/* =========  NPB PC NPButtons methods  ========= */
static void npb_pc_npbuttons_init() {
  windiv_init(&(nplayerbox->playercontrols.npbuttons.wd));

  npb_pc_npbuttons__set(
		  NPB_PC_NPBUTTONS__STR_UNTOUCHABLE,
		  NPB_PC_NPBUTTONS_PLAY_STR_DEF,
		  NPB_PC_NPBUTTONS__STR_UNTOUCHABLE);

  npb_pc_npbuttons_draw();
}

static void npb_pc_npbuttons_finalize() {
  windiv_finalize(&(nplayerbox->playercontrols.npbuttons.wd));
}

static void npb_pc_npbuttons_draw() {
  int pc_width = windiv_getwidth(nplayerbox->playercontrols.wd);
  int pc_time_width = windiv_getwidth(nplayerbox->playercontrols.time.wd_labels);
  int req_x = (pc_width - NPB_PC_NPBUTTONS_WIDTH_MINIMAL)/2;
  int npbuttons_x = req_x;

  switch(nscreen.scaling) {
  case NSCREEN_SCALE_OUT:
    break;

  case NSCREEN_SCALE_MINIMAL:
    if (npbuttons_x < pc_time_width)
      npbuttons_x = pc_time_width;
  default:
    windiv_create_with_paddings(&(nplayerbox->playercontrols.npbuttons.wd), (windiv_parent) &(nplayerbox->playercontrols.wd),
      NPB_PC_NPBUTTONS_HEIGHT_MINIMAL, NPB_PC_NPBUTTONS_WIDTH_MINIMAL,
      -1,
      npbuttons_x,
      0, 0, 1, 1);
    npb_pc_npbuttons__refresh();
  }
}


/* ===========  NPB PC Volume methods  =========== */
static void npb_pc_volume_init() {
  windiv_init(&(nplayerbox->playercontrols.volume.wd));

  nplayerbox->playercontrols.volume.current = NPB_PC_VOLUME_MAX_DEF*0.8;
  nplayerbox->playercontrols.volume.maximum = NPB_PC_VOLUME_MAX_DEF;

  npb_pc_volume_draw();
}

static void npb_pc_volume_finalize() {
  windiv_finalize(&(nplayerbox->playercontrols.volume.wd));
}

static void npb_pc_volume_draw() {
  /*switch(nscreen.scaling) {
  case NSCREEN_SCALE_OUT:
    break;
  default:
    break;
  }*/
}


/* Setters e refreshers do NPButtons :) */
void npb_pc_npbuttons__set(const char *prev, const char *nplay, const char *next) {
  nplayerbox->playercontrols.npbuttons.prev = prev;
  nplayerbox->playercontrols.npbuttons.nplay = nplay;
  nplayerbox->playercontrols.npbuttons.next = next;
}

void npb_pc_npbuttons__refresh() {
  mvwprintw(nplayerbox->playercontrols.npbuttons.wd.win, 0, 0, " %s  %s  %s ",
		  nplayerbox->playercontrols.npbuttons.prev, nplayerbox->playercontrols.npbuttons.nplay,
		  nplayerbox->playercontrols.npbuttons.next);
  wrefresh(nplayerbox->playercontrols.npbuttons.wd.win);
}
